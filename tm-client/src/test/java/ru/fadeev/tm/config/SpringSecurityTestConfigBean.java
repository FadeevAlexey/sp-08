package ru.fadeev.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.entity.SessionUser;

import static ru.fadeev.tm.util.TestUtil.*;

@TestConfiguration
@Service("userDetailsTest")
public class SpringSecurityTestConfigBean implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        @NotNull UserDTO user = user();
        if (username.equals("Admin")) user = admin();
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.password(user.getPassword());
        builder.roles(user.getRole().toString());
        UserDetails details = builder.build();
        org.springframework.security.core.userdetails.User result = null;
        result = (org.springframework.security.core.userdetails.User) details;
        final SessionUser sessionUser = new SessionUser(result);
        sessionUser.setUserId(user.getId());
        return sessionUser;
    }

}