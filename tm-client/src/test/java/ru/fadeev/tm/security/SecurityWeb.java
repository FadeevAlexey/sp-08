package ru.fadeev.tm.security;

import org.junit.Test;
import org.springframework.security.test.context.support.WithUserDetails;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.fadeev.tm.util.TestUtil.*;

public class SecurityWeb extends AbstractSecurityTest {

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void getUsersWeb() throws Exception {
        mvc.perform(get("/admin/users/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("user_list"))
                .andExpect(forwardedUrl("/WEB-INF/user_list.jsp"));
    }

    @Test
    public void unAuth() throws Exception {
        mvc.perform(get("/users"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithUserDetails(value = "User", userDetailsServiceBeanName = "userDetailsService")
    public void forbidden() throws Exception {
        mvc.perform(get("/admin/users")
                .with(userHttpBasic(USER)))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

}