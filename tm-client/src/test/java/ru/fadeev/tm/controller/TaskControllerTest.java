package ru.fadeev.tm.controller;

import org.junit.Assert;
import org.junit.Test;

import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.web.util.NestedServletException;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.util.TestUtil;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TaskControllerTest extends AbstractController {

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void tasksViewTest() throws Exception {
        mvc.perform(get("/task"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("task_list"))
                .andExpect(model().attributeExists("login", "tasks"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void taskCreateTestGet() throws Exception {
        mvc.perform(get("/task/create"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("task_create"))
                .andExpect(model().attributeExists("login", "projects"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void taskCreateTestPost() throws Exception {
        mvc.perform(post("/task/create").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(TestUtil.buildUrlEncodedFormEntity(
                        "name", "testName",
                        "description", "testDescription"
                )))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/task?login=Admin"));

    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void taskRemoveTest() throws Exception {
        TaskDTO task = new TaskDTO();
        mvc.perform(get("/task/remove/" + task.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/task"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void taskViewTest() throws Exception {
        final TaskDTO task = new TaskDTO();
        try {
            mvc.perform(get("/task/view/" + task.getId()));
        } catch (NestedServletException e) {
            Assert.assertTrue(e.getMessage().contains("404"));
        }
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void taskEditGet() throws Exception {
        final TaskDTO task = new TaskDTO();
        try {
            mvc.perform(get("/task/edit/" + task.getId()));
        } catch (NestedServletException e){
            Assert.assertTrue(e.getMessage().contains("404"));
        }

    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void taskEditPost() throws Exception {
        final TaskDTO task = new TaskDTO();
        mvc.perform(post("/task/create").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(TestUtil.buildUrlEncodedFormEntity(
                        "name", "testName",
                        "description", "testDescription"
                )))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/task?login=Admin"));
    }

}