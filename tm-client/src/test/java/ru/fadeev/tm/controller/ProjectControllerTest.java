package ru.fadeev.tm.controller;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.web.util.NestedServletException;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.util.TestUtil;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProjectControllerTest extends AbstractController {

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void projectsViewTest() throws Exception {
        mvc.perform(get("/project"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project_list"))
                .andExpect(model().attributeExists("login", "projects"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void projectCreateTestGet() throws Exception {
        mvc.perform(get("/project/create"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project_create"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void projectCreateTestPost() throws Exception {
        mvc.perform(post("/project/create").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(TestUtil.buildUrlEncodedFormEntity(
                        "name", "testName",
                        "description", "testDescription"
                )))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/project?login=Admin"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void projectRemoveTest() throws Exception {
        final ProjectDTO project = new ProjectDTO();
        mvc.perform(get("/project/remove/" + project.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/project"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void projectEditGet() throws Exception {
        final ProjectDTO project = new ProjectDTO();
        try {
            mvc.perform(get("/project/edit/" + project.getId()));
        } catch (NestedServletException e){
            Assert.assertTrue(e.getMessage().contains("404"));
        }
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void projectEditPost() throws Exception {
        final ProjectDTO project = new ProjectDTO();
        mvc.perform(post("/project/edit/" + project.getId()).contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(TestUtil.buildUrlEncodedFormEntity(
                        "name", "testNameChange",
                        "description", "testDescriptionChange"
                )))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/project?login=Admin"));
    }

}