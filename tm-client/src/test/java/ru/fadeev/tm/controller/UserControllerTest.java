package ru.fadeev.tm.controller;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import ru.fadeev.tm.util.TestUtil;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserControllerTest extends AbstractController {

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void userViewTest() throws Exception {
        mvc.perform(get("/user/view"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("user_view"))
                .andExpect(model().attribute("login", "Admin"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void userEditTest() throws Exception {
        mvc.perform(get("/user/edit"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("user_edit"))
                .andExpect(model().attribute("login", "Admin"))
                .andReturn().getModelAndView();
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void userEditPost() throws Exception {
        mvc.perform(post("/task/create").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(TestUtil.buildUrlEncodedFormEntity(
                        "name", "testName",
                        "description", "testDescription"
                )))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/task?login=Admin"));

    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void userAdminUsersTest() throws Exception {
        mvc.perform(get("/admin/users"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("user_list"))
                .andExpect(model().attributeExists("users", "login"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void userAdminViewTest() throws Exception {
        mvc.perform(get("/admin/view/" + TestUtil.user().getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("user_view"))
                .andExpect(model().attribute("login", "User"));
    }


    @Test
    public void userCreateTestPost() throws Exception {
        mvc.perform(post("/task/create").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(TestUtil.buildUrlEncodedFormEntity(
                        "name", "testName",
                        "description", "testDescription"
                )))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/login"));
    }

}