package ru.fadeev.tm.restcontroller;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.fadeev.tm.dto.UserDTO;

import java.util.UUID;

public class UserRestApiTest extends AbstractRestApiTest {

    @Test
    public void persistUserTest() {
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(UUID.randomUUID().toString());
        client.createUser(userDTO);
        @NotNull final UserDTO userResult = client.getUserAdmin(adminProfile, userDTO.getId());
        Assert.assertEquals(userDTO.getLogin(), userResult.getLogin());
        client.deleteUserAdmin(adminProfile, userDTO.getId());
    }

    @Test
    public void findAllUserTest() {
        final int startTest = client.findAll(adminProfile).size();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(UUID.randomUUID().toString());
        client.createUser(user);
        final int finishTest = client.findAll(adminProfile).size();
        Assert.assertEquals(startTest, finishTest - 1);
        Assert.assertNotNull(client.getUserAdmin(adminProfile, user.getId()));
        client.deleteUserAdmin(adminProfile, user.getId());
    }

    @Test
    public void getOneUserTestAdmin() {
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(UUID.randomUUID().toString());
        client.createUser(user);
        @NotNull final UserDTO testUser = client.getUserAdmin(adminProfile, user.getId());
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
        client.deleteUserAdmin(adminProfile, user.getId());
    }

    @Test
    public void removeUserAdmin() {
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(UUID.randomUUID().toString());
        client.createUser(user);
        Assert.assertEquals(user.getLogin(), client.getUserAdmin(adminProfile, user.getId()).getLogin());
        client.deleteUserAdmin(adminProfile, user.getId());
        try {
            client.getUserAdmin(adminProfile, user.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
    }

    @Test
    public void mergeUserTest() {
        @NotNull UserDTO user = new UserDTO();
        user.setLogin("testU");
        user.setPassword("1");
        client.createUser(user);
        @NotNull UserDTO testUser = client.getUserAdmin(adminProfile, user.getId());
        testUser.setEmail("1@1.ru");
        client.updateUser(user.getId(), testUser);
        testUser = client.getUserAdmin(adminProfile, testUser.getId());
        Assert.assertEquals("1@1.ru", testUser.getEmail());
        client.deleteUserAdmin(adminProfile, testUser.getId());
    }

    @Test
    public void removeUserTest() {
        @NotNull UserDTO user = new UserDTO();
        user.setLogin("testU");
        user.setPassword("1");
        client.createUser(user);
        @NotNull UserDTO testUser = client.getUserAdmin(adminProfile, user.getId());
        testUser.setEmail("1@1.ru");
        client.updateUser(user.getId(), testUser);
        testUser = client.getUserAdmin(adminProfile, testUser.getId());
        Assert.assertEquals("1@1.ru", testUser.getEmail());
        client.deleteUser(user.getId());
        @Nullable UserDTO result = null;
        try {
            result = client.getUserAdmin(adminProfile, user.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        Assert.assertNull(result);
    }

}
