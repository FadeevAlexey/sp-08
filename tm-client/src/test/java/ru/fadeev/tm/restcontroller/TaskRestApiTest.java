package ru.fadeev.tm.restcontroller;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.fadeev.tm.dto.TaskDTO;

public class TaskRestApiTest extends AbstractRestApiTest {

    @Test
    public void persistTaskTest() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("testUser");
        client.createTask(userProfile, task);
        @NotNull final TaskDTO taskResult = client.getTask(userProfile, task.getId());
        Assert.assertEquals("testUser", taskResult.getName());
        client.deleteTask(userProfile, task.getId());
    }

    @Test
    public void findAllTaskTest() {
        final int startTest = client.findAllTask(userProfile).size();
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        client.createTask(userProfile, task);
        client.createTask(userProfile, task2);
        client.createTask(userProfile, task3);
        final int finishTest = client.findAll(userProfile).size();
        Assert.assertEquals(startTest, finishTest - 3);
        Assert.assertNotNull(client.getTask(userProfile, task.getId()));
        @Nullable TaskDTO taskDTO = null;
        try {
            taskDTO = client.getTask(adminProfile, task.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        Assert.assertNull(taskDTO);
        client.deleteTask(userProfile, task.getId());
        client.deleteTask(userProfile, task2.getId());
        client.deleteTask(userProfile, task3.getId());
    }

    @Test
    public void getOneTaskTest() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        client.createTask(userProfile, task);
        @NotNull final TaskDTO testTask = client.getTask(userProfile, task.getId());
        Assert.assertEquals(task.getName(), testTask.getName());
        @Nullable TaskDTO taskDTO = null;
        try {
            taskDTO = client.getTask(adminProfile, task.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        Assert.assertNull(taskDTO);
        client.deleteTask(userProfile, task.getId());
    }

    @Test
    public void mergeTaskTest() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        client.createTask(userProfile, task);
        @NotNull TaskDTO testTask = client.getTask(userProfile, task.getId());
        testTask.setDescription("test description");
        client.updateTask(userProfile, testTask);
        testTask = client.getTask(userProfile, testTask.getId());
        Assert.assertEquals("test description", testTask.getDescription());
        @Nullable TaskDTO taskDTO = null;
        try {
            taskDTO = client.getTask(adminProfile, task.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        Assert.assertNull(taskDTO);
        client.deleteTask(userProfile, task.getId());
    }

    @Test
    public void removeTaskTest() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        client.createTask(userProfile, task);
        Assert.assertEquals("test", client.getTask(userProfile, task.getId()).getName());
        client.deleteTask(userProfile, task.getId());
        try {
            client.getTask(userProfile, task.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
    }

}
