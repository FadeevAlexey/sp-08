package ru.fadeev.tm.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.fadeev.tm.client.Client;

import static ru.fadeev.tm.util.TestUtil.admin;
import static ru.fadeev.tm.util.TestUtil.user;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractRestApiTest {

    @Autowired
    protected Client client;

    @NotNull
    final protected String userProfile = user().getId();

    @NotNull
    final protected String adminProfile = admin().getId();

}
