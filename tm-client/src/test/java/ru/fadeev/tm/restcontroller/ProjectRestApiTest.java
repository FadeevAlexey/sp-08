package ru.fadeev.tm.restcontroller;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;

import org.junit.Test;
import ru.fadeev.tm.dto.ProjectDTO;

import static ru.fadeev.tm.util.TestUtil.*;

public class ProjectRestApiTest extends AbstractRestApiTest {

    @Test
    public void persistProjectTest() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("testProject");
        client.createProject(admin().getId(), project);
        @NotNull final ProjectDTO result = client.getProject(admin().getId(), project.getId());
        Assert.assertNotNull(result);
        Assert.assertEquals("testProject", result.getName());
        client.deleteProject(admin().getId(), project.getId());
    }


    @Test
    public void findAllProjectTest() {
        final int startTest = client.findAllProject(userProfile).size();
        @NotNull ProjectDTO project = new ProjectDTO();
        @NotNull ProjectDTO project2 = new ProjectDTO();
        @NotNull ProjectDTO project3 = new ProjectDTO();
        client.createProject(userProfile, project);
        client.createProject(userProfile, project2);
        client.createProject(userProfile, project3);
        final int finishTest = client.findAllProject(userProfile).size();
        Assert.assertEquals(startTest, finishTest - 3);
        Assert.assertNotNull(client.getProject(userProfile, project.getId()));
        @Nullable ProjectDTO projectDTO = null;
        try {
            projectDTO = client.getProject(adminProfile, project.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        client.deleteProject(userProfile, project.getId());
        client.deleteProject(userProfile, project2.getId());
        client.deleteProject(userProfile, project3.getId());
    }

    @Test
    public void getOneProjectTest() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("test");
        client.createProject(userProfile, project);
        @NotNull final ProjectDTO testProject = client.getProject(userProfile, project.getId());
        Assert.assertEquals(project.getName(), testProject.getName());
        @Nullable ProjectDTO projectDTO = null;
        try {
            projectDTO = client.getProject(adminProfile, project.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        Assert.assertNull(projectDTO);
        client.deleteProject(userProfile, project.getId());
    }

    @Test
    public void mergeProjectTest() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("test");
        client.createProject(userProfile, project);
        @NotNull ProjectDTO testProject = client.getProject(userProfile, project.getId());
        testProject.setDescription("test description");
        client.updateProject(userProfile, testProject);
        testProject = client.getProject(userProfile, testProject.getId());
        Assert.assertEquals("test description", testProject.getDescription());
        @Nullable ProjectDTO projectDTO = null;
        try {
            projectDTO = client.getProject(adminProfile, project.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        Assert.assertNull(projectDTO);
        client.deleteProject(userProfile, project.getId());
    }

    @Test
    public void removeProjectTest() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("test");
        client.createProject(userProfile, project);
        Assert.assertEquals("test", client.getProject(userProfile, project.getId()).getName());
        client.deleteProject(userProfile, project.getId());
        try {
            client.getProject(userProfile, project.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
    }

}
