package ru.fadeev.tm.util;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.enumerated.RoleType;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class TestUtil {

    public static RequestPostProcessor userHttpBasic(UserDTO user) {
        return SecurityMockMvcRequestPostProcessors.httpBasic(user.getLogin(), user.getPassword());
    }

    public static RequestPostProcessor userAuth(UserDTO user) {
        return SecurityMockMvcRequestPostProcessors.authentication(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
    }

    public static String buildUrlEncodedFormEntity(String... params) {
        if ((params.length % 2) > 0) {
            throw new IllegalArgumentException("Need to give an even number of parameters");
        }
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < params.length; i += 2) {
            if (i > 0) {
                result.append('&');
            }
            try {
                result.
                        append(URLEncoder.encode(params[i], StandardCharsets.UTF_8.name())).
                        append('=').
                        append(URLEncoder.encode(params[i + 1], StandardCharsets.UTF_8.name()));
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
        return result.toString();
    }

    public static UserDTO admin() {
        UserDTO admin = new UserDTO();
        admin.setLogin("Admin");
        admin.setId("9070cf94-9500-4bf2-bc0e-ece81dd76870");
        admin.setPassword("$2a$10$aJFwoRVcvvR2jTrf.0uh7OB0kGEexiMYW2VexzhLvtVUZ3F8QrCd.");
        admin.setRole(RoleType.ADMINISTRATOR);
        return admin;
    }

    public static UserDTO user() {
        UserDTO user = new UserDTO();
        user.setId("951f973c-8ae3-42eb-b7c4-0c4b3ef44161");
        user.setLogin("User");
        user.setPassword("$2a$10$7zy3NjrcGclhf7o6RBoO5OfBovP2iceAFikMB4sq5FLKGFRWEVM7S");
        user.setRole(RoleType.USER);
        return user;
    }

}
