package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.fadeev.tm.client.Client;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.entity.SessionUser;
import ru.fadeev.tm.enumerated.RoleType;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private Client client;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final @NotNull UserDTO user = findByLogin(username);
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.password(user.getPassword());
        final RoleType userRole = user.getRole();
        builder.roles(userRole.toString());
        UserDetails details = builder.build();

        org.springframework.security.core.userdetails.User result = null;
        result = (org.springframework.security.core.userdetails.User) details;

        final SessionUser sessionUser = new SessionUser(result);
        sessionUser.setUserId(user.getId());
        return sessionUser;
    }

    private UserDTO findByLogin(@NotNull final String username) {
        return client.findUserByName(username);
    }

}