package ru.fadeev.tm.client;

import feign.Feign;
import feign.Headers;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.dto.UserDTO;

import java.util.List;

@FeignClient(name = "tm-server")
@Headers("Content-Type: application/json")
public interface Client {

    @NotNull
    @GetMapping("api/projects")
    List<ProjectDTO> findAllProject(@RequestParam("userId") @NotNull String userId);

    @PostMapping("api/project")
    ProjectDTO createProject(@RequestParam("userId") @NotNull String userId, @NotNull ProjectDTO projectDTO);

    @GetMapping("api/project/{id}")
    ProjectDTO getProject(@RequestParam("userId") @NotNull String userId, @PathVariable(name = "id") @NotNull String id);

    @DeleteMapping("api/project/{id}")
    void deleteProject(@RequestParam("userId") @NotNull String userId, @PathVariable(name = "id") @NotNull String id);

    @PutMapping("api/project")
    ProjectDTO updateProject(@RequestParam("userId") @NotNull String userId, final ProjectDTO projectDTO);

    @NotNull
    @GetMapping("api/tasks")
    List<TaskDTO> findAllTask(@RequestParam("userId") @NotNull String userId);

    @PostMapping("api/task")
    TaskDTO createTask(@RequestParam("userId") @NotNull String userId, @NotNull TaskDTO taskDTO);

    @GetMapping("api/task/{id}")
    TaskDTO getTask(@RequestParam("userId") @NotNull String userId, @PathVariable(name = "id") @NotNull String id);

    @DeleteMapping("api/task/{id}")
    void deleteTask(@RequestParam("userId") @NotNull String userId, @PathVariable(name = "id") @NotNull String id);

    @PutMapping("api/task")
    TaskDTO updateTask(@RequestParam("userId") @NotNull String userId, final TaskDTO taskDTO);

    @NotNull
    @GetMapping("api/users")
    List<UserDTO> findAll(@RequestParam("userId") @NotNull String userId);

    @PostMapping("api/user/create")
    UserDTO createUser(@NotNull UserDTO userDTO);

    @GetMapping("api/user/{id}")
    UserDTO getUserAdmin(@RequestParam("userId") @NotNull String userId, @PathVariable(name = "id") @NotNull String id);

    @DeleteMapping("api/user/{id}")
    void deleteUserAdmin(@RequestParam("userId") @NotNull String userId, @PathVariable(name = "id") @NotNull String id);

    @DeleteMapping("api/user")
    void deleteUser(@RequestParam("userId") @NotNull String userId);

    @PutMapping("api/user")
    UserDTO updateUser(@RequestParam("userId") @NotNull String userId, UserDTO userDTO);

    @GetMapping("api/user/name/{login}")
    UserDTO findUserByName(@PathVariable(name = "login") @NotNull String login);

}