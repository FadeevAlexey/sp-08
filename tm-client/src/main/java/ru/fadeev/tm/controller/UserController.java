package ru.fadeev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.client.Client;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.entity.SessionUser;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private Client client;

    @PostMapping("/registration")
    public ModelAndView createUser(@NotNull final UserDTO userDTO) {
        @NotNull final ModelAndView model = new ModelAndView("redirect:/");
        client.createUser(userDTO);
        return model;
    }

    @GetMapping("/user/view")
    public ModelAndView viewUser(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final ModelAndView model = new ModelAndView("user_view");
        model.addObject("login", sessionUser.getUsername());
        model.addObject("projectsCount", client.findAllProject(sessionUser.getUserId()).size());
        model.addObject("tasksCount", client.findAllTask(sessionUser.getUserId()).size());
        return model;
    }

    @GetMapping("/user/edit")
    public ModelAndView editUser(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final ModelAndView model = new ModelAndView("user_edit");
        model.addObject("login", sessionUser.getUsername());
        return model;
    }

    @PostMapping("/user/edit")
    public ModelAndView editUserPost(
            @NotNull final Authentication authentication,
            @NotNull final UserDTO userDTO) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        userDTO.setId(sessionUser.getUserId());
        client.updateUser(sessionUser.getUserId(), userDTO);
        @NotNull final ModelAndView model = new ModelAndView("redirect:/");
        return model;
    }

    @GetMapping(value = "/user/remove")
    public ModelAndView userRemoveGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        client.deleteUser(userId);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/logout");
        modelAndView.addObject("login", sessionUser.getUsername());
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/admin/users")
    public ModelAndView userListGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final List<UserDTO> users = client.findAll(sessionUser.getUserId());
        @NotNull final ModelAndView model = new ModelAndView("user_list");
        model.addObject("login", sessionUser.getUsername());
        model.addObject("users", users);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping("/admin/view/{id}")
    public ModelAndView adminView(
            @NotNull final Authentication authentication,
            @PathVariable String id) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @Nullable UserDTO user = client.getUserAdmin(sessionUser.getUserId(), id);
        if (user == null) return new ModelAndView("redirect:/");
        @NotNull final ModelAndView model = new ModelAndView("user_view");
        model.addObject("login", user.getLogin());
        return model;
    }

    @GetMapping("/admin/remove/{id}")
    public ModelAndView adminRemove(
            @NotNull final Authentication authentication,
            @PathVariable String id) {
        @NotNull final ModelAndView model = new ModelAndView("redirect:/admin/users");
        client.deleteUser(id);
        return model;
    }

}