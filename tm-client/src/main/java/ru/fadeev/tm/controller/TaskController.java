package ru.fadeev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.client.Client;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.entity.SessionUser;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private Client client;

    @GetMapping(value = "/task")
    public ModelAndView taskListGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @NotNull final List<TaskDTO> tasks = client.findAllTask(userId);
        @NotNull final ModelAndView model = new ModelAndView("task_list");
        tasks.forEach(x-> System.out.println(x.getProjectName()));
        model.addObject("login", sessionUser.getUsername());
        model.addObject("tasks", tasks);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/task/create")
    public ModelAndView taskCreateGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @NotNull final List<String> projects = new ArrayList<>();
        client.findAllProject(userId).forEach(project -> projects.add(project.getName()));
        @NotNull final ModelAndView model = new ModelAndView("task_create");
        model.addObject("login", sessionUser.getUsername());
        model.addObject("projects", projects);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/task/create")
    public ModelAndView taskCreatePost(
            @NotNull final Authentication authentication,
            @NotNull final TaskDTO taskDTO
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        taskDTO.setUserId(userId);
        client.createTask(userId,taskDTO);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/task");
        modelAndView.addObject("login", sessionUser.getUsername());
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/task/remove/{id}")
    public ModelAndView TaskRemoveGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        client.deleteTask(userId, id);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/task");
        modelAndView.addObject("login", sessionUser.getUsername());
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/task/view/{id}")
    public ModelAndView taskViewGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @Nullable final TaskDTO task = client.getTask(userId, id);
        @NotNull final ModelAndView model = new ModelAndView("task_view");
        model.addObject("login", sessionUser.getUsername());
        if (task == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        model.addObject("task", task);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/task/edit/{id}")
    public ModelAndView taskEditGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id
    ) {
        @NotNull final ModelAndView model = new ModelAndView("task_edit");
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        model.addObject("login", sessionUser.getUsername());
        @NotNull final String userId = sessionUser.getUserId();
        @NotNull final List<String> projects = new ArrayList<>();
        client.findAllProject(userId).forEach(project -> projects.add(project.getName()));
        @Nullable final TaskDTO task = client.getTask(userId, id);
        if (task == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        @Nullable final TaskDTO taskDTO = task;
        model.addObject("task", taskDTO);
        model.addObject("projects", projects);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/task/edit/{id}")
    public ModelAndView taskEditPost(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id,
            @NotNull final TaskDTO taskDTO
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        taskDTO.setUserId(userId);
        taskDTO.setId(id);
        @NotNull ModelAndView model = new ModelAndView("redirect:/task");
        model.addObject("login", sessionUser.getUsername());
        client.updateTask(userId,taskDTO);
        model.setStatus(HttpStatus.OK);
        return model;
    }

}