package ru.fadeev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.client.Client;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.entity.SessionUser;

import java.util.List;

@Controller
public class ProjectController {

    @Autowired
    private Client client;

    @GetMapping(value = "/project")
    public ModelAndView projectListGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @NotNull final List<ProjectDTO> projects = client.findAllProject(userId);
        @NotNull final ModelAndView model = new ModelAndView("project_list");
        model.addObject("login", sessionUser.getUsername());
        model.addObject("projects", projects);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/project/create")
    public ModelAndView projectCreateGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final ModelAndView model = new ModelAndView("project_create");
        model.addObject("login", sessionUser.getUsername());
        model.addObject("login", sessionUser.getUsername());
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/project/create")
    public ModelAndView projectCreatePost(
            @NotNull final Authentication authentication,
            @NotNull final ProjectDTO projectDTO
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        projectDTO.setUserId(userId);
        client.createProject(userId, projectDTO);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/project");
        modelAndView.addObject("login", sessionUser.getUsername());
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/project/remove/{id}")
    public ModelAndView projectRemoveGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        client.deleteProject(userId, id);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/project");
        modelAndView.addObject("login", sessionUser.getUsername());
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/project/view/{id}")
    public ModelAndView projectViewGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @Nullable final ProjectDTO project = client.getProject(userId, id);
        @NotNull final ModelAndView model = new ModelAndView("project_view");
        model.addObject("login", sessionUser.getUsername());
        if (project == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        model.addObject("project", project);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/project/edit/{id}")
    public ModelAndView projectEditGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id
    ) {
        @NotNull final ModelAndView model = new ModelAndView("project_edit");
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @Nullable final ProjectDTO project = client.getProject(userId, id);
        if (project == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        @Nullable final ProjectDTO projectDTO = (project);
        model.addObject("login", sessionUser.getUsername());
        model.addObject("project", projectDTO);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/project/edit/{id}")
    public ModelAndView projectEditPost(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull String id,
            @NotNull final ProjectDTO projectDTO
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        projectDTO.setId(id);
        projectDTO.setUserId(userId);
        @NotNull ModelAndView model = new ModelAndView("redirect:/project");
        model.addObject("login", sessionUser.getUsername());
        client.updateProject(userId, projectDTO);
        model.setStatus(HttpStatus.OK);
        return model;
    }

}