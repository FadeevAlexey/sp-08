<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="material design, button, material button, css3, html5">
    <link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons'>
    <!--===============================================================================================-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/header-search.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/form.css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>
<body>
<header class="header-search">
    <div class="header-limiter">
        <h1><a href="${pageContext.request.contextPath}/">TASK<span>MANAGER</span></a></h1>
        <nav>
            <a href="${pageContext.request.contextPath}/">Home</a>
            <a href="${pageContext.request.contextPath}/project">Projects</a>
            <a href="${pageContext.request.contextPath}/task">Tasks</a>
            <sec:authorize access="hasRole('ADMINISTRATOR')">
                <a href="${pageContext.request.contextPath}/admin/users">Users</a>
            </sec:authorize>
        </nav>
        <form method="get" class="selected">
            <a href="${pageContext.request.contextPath}/user/view">${login}</a>
        </form>
    </div>
</header>
<div class="container">
    <h1>User</h1>

    <h3>Project Name</h3>
    <p>${login}</p>

    <h3>Tasks count</h3>
    <p>${tasksCount}</p>

    <h3>Projects count</h3>
    <p>${projectsCount}</p>

    <h3><a href="${pageContext.request.contextPath}/logout">Logout</a></h3>

    <button class="rkmd-btn btn-fab btn-green ripple-effect" onclick="window.history.go(-1); return false;"><i
            class="material-icons">keyboard_backspace</i></button>
    <button class="rkmd-btn btn-fab btn-orange ripple-effect" onclick="document.location='edit'"><i
            class="material-icons">edit</i></button>
    <button class="rkmd-btn btn-fab btn-red ripple-effect" onclick="document.location='remove'"><i
            class="material-icons">delete</i></button>
</div>
</body>
</html>