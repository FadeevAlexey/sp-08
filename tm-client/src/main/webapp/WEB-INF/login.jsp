<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>

<html lang="en" >
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Pacifico'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons'>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">

</head>
<body>
<html>
<head>
    <meta charset="utf-8">
</head>
<body class="login-body">
<div class="row">
    <div class="input-cart col s12 m10 push-m1 z-depth-2 grey lighten-5">
        <div class="col s12 m5 login">
            <h4 class="center">Log in</h4>
            <h5 class="center">${loginExist}</h5>
            <br>
            <form action="/login" method="post">
                <div class="row">
                    <div class="input-field">
                        <input type="text" id="user" name="username" class="validate" required="required" placeholder="Username">
                        <label for="user">
                            <i class="material-icons">person</i>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field">
                        <input type="password" id="pass" name="password" class="validate" required="required" placeholder="Password">
                        <label for="pass">
                            <i class="material-icons">lock</i>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col s6">
                        <button type="submit" name="login" class="btn waves-effect waves-light blue right">Log in</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- Signup form -->
        <div class="col s12 m7 signup">
            <div class="signupForm">
                <h4 class="center">Sign up</h4>
                <br>
                <form action="/registration" method="post">
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <input type="text" id="name-picked" name="login" class="validate" required="required" placeholder="Enter a username">
                            <label for="name-picked">
                                <i class="material-icons">person_add</i>
                            </label>
                        </div>
                        <div class="input-field col s12 m6">
                            <input type="password" id="pass-picked" name="password" class="validate" required="required" placeholder="Password">
                            <label for="pass-picked">
                                <i class="material-icons">lock</i>                    </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field email">
                            <div class="col s12">
                                <input type="text" id="email" name="email" class="validate" required="required" placeholder="Enter your email">
                                <label for="email">
                                    <i class="material-icons">mail</i>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button type="submit" name="btn-signup" class="btn blue right waves-effect waves-light">Sign Up</button>
                    </div>
                </form>
            </div>
            <div class="signup-toggle center" >
                <h4 class="center">Have No Account ? <a href="#!">Sign Up</a></h4>
            </div>
        </div>
        <div class="col s12">
            <br>
        </div>
    </div>
</div>
<div class="fixed-action-btn toolbar">
    <a class="btn-floating btn-large amber black-text">
        Login
    </a>
</div>
</body>
</html>
<!-- partial -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js'>

</script><script  src="${pageContext.request.contextPath}/js/script.js"></script>

</body>
</html>