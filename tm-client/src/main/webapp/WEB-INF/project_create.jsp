<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/header-search.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/form.css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>
<body>
<header class="header-search">
    <div class="header-limiter">
        <h1><a href="${pageContext.request.contextPath}/">TASK<span>MANAGER</span></a></h1>
        <nav>
            <a href="${pageContext.request.contextPath}/">Home</a>
            <a href="#" class="selected">Projects</a>
            <a href="${pageContext.request.contextPath}/task">Tasks</a>
            <sec:authorize access="hasRole('ADMINISTRATOR')">
                <a href="${pageContext.request.contextPath}/admin/users">Users</a>
            </sec:authorize>
        </nav>
        <form method="get" class="selected">
            <a href="${pageContext.request.contextPath}/user/view">${login}</a>
        </form>
    </div>
</header>
<div class="container">
    <form method="POST" action="/project/create">
        <h1>Project create</h1>
        <div class="form-group">
            <input type="text" required="required" name="name"/>
            <label for="input" class="control-label">Name</label><i class="bar"></i>
        </div>
        <div class="form-group">
            <textarea name="description"></textarea>
            <label for="textarea" class="control-label">Description</label><i class="bar"></i>
        </div>
        <div class="form-group">
            <input id="date" type="date" name="startDate">
            <label for="input" class="control-label">Begin date</label><i class="bar"></i>
        </div>
        <div class="form-group">
            <input id="date" type="date" name="finishDate">
            <label for="input" class="control-label">End date</label><i class="bar"></i>
        </div>
        <div class="form-radio">
            <h3>Status</h3>
            <div class="radio">
                <label>
                    <input type="radio" name="status" checked="checked" value="PLANNED"/>Planned<i class="helper"></i>
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="status" value="IN_PROGRESS"/><i class="helper"></i>In Progress
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="status" value="DONE"/><i class="helper"></i>Done
                </label>
            </div>
        </div>
        <div class="button-container">
            <button type="submit" class="button"><span>Submit</span></button>
        </div>
    </form>
</div>
</body>
</html>