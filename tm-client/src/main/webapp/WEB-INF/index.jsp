<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css'>
    <link href="css/header-search.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/str.css"/>
</head>
<body>
<header class="header-search">
    <div class="header-limiter">
        <h1><a href="${pageContext.request.contextPath}">TASK<span>MANAGER</span></a></h1>
        <nav>
            <a href="${pageContext.request.contextPath}/" class="selected">Home</a>
            <a href="${pageContext.request.contextPath}/project">Projects</a>
            <a href="${pageContext.request.contextPath}/task">Tasks</a>
            <sec:authorize access="hasRole('ADMINISTRATOR')">
                <a href="${pageContext.request.contextPath}/admin/users">Users</a>
            </sec:authorize>
        </nav>
    </div>
</header>
<div id="demo">
    <h1> Task Manager for today's workplace.</h1>
</div>
<div class="as-container">
    <div class="as-choice-card" onClick="document.location='project'">
        <i class="as-card-icon fas fa-calendar"></i>
        <h3 class="as-card-title">
            Transform your PROJECT list
            into a unique workflow
        </h3>
        <p class="as-card-info">
            Easily manage your work
        </p>
    </div>
    <div class="as-choice-card" onClick="document.location='task'">
        <i class="as-card-icon fas fa-tasks"></i>
        <h3 class="as-card-title">
            Organize your TASK efficiently
        </h3>
        <p class="as-card-info">
            Task organization for productivity growth
        </p>
    </div>
</div>
</body>
</html>