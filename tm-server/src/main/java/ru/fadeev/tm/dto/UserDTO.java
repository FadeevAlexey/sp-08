package ru.fadeev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.RoleType;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper = true)
public final class UserDTO extends AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = -7402071995321582277L;

    public UserDTO(@Nullable String login, @NotNull String password) {
        this.login = login;
        this.password = password;
    }

    public UserDTO(@NotNull final User user) {
        setLogin(user.getLogin());
        setEmail(user.getEmail());
        setId(user.getId());
        if (!user.getRoles().isEmpty()) setRole(user.getRoles().get(0).getRole());
        setPassword(user.getPasswordHash());
    }

    @Nullable
    private String login = "";

    @NotNull
    private String password = "";

    @NotNull
    private RoleType role = RoleType.USER;

    @Nullable
    private String email = "";

}