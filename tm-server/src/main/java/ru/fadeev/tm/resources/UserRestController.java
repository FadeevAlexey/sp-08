package ru.fadeev.tm.resources;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.entity.User;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserRestController {

    @NotNull
    @Autowired
    private IUserService userService;

    @GetMapping(value = "/api/users")
    public ResponseEntity<List<UserDTO>> getUsers(@NotNull final String userId) {
        User user = userService.findOne(userId);
        if(user == null) return ResponseEntity.notFound().build();
        if (!user.getRoles().get(0).getRole().toString().equals("ADMINISTRATOR"))
            return ResponseEntity.badRequest().build();
        @NotNull final List<UserDTO> users = userService.findAll().stream()
                .map(UserDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok(users);
    }

    @PostMapping(value = "/api/user/create")
    public ResponseEntity<UserDTO> createUser(
            @RequestBody @NotNull final UserDTO userDTO
    ) {
        @NotNull final User user = userService.convertToUser(userDTO);
        userService.persist(user);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/api/user/{id}")
    public ResponseEntity<UserDTO> deleteUserAdmin(
            @PathVariable @NotNull final String id
    ) {
        userService.remove(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/api/user")
    public ResponseEntity<UserDTO> deleteUser(
            @NotNull final String userId
    ) {
        userService.remove(userId);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/api/user/{id}")
    public ResponseEntity<UserDTO> userView(
            @PathVariable @NotNull final String id
    ) {
        @Nullable final User user = userService.findOne(id);
        if (user == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(new UserDTO(user));
    }

    @PutMapping(value = "/api/user")
    public ResponseEntity<UserDTO> userEdit(
            @NotNull final String userId,
            @RequestBody @Nullable UserDTO userDTO
    ) {
        if (userDTO == null) return ResponseEntity.badRequest().build();
        userDTO.setId(userId);
        @NotNull final User user = userService.convertToUser(userDTO);
        userService.merge(user);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/api/user/name/{login}")
    public ResponseEntity<UserDTO> getUserByName(
            @PathVariable String login) {
        User user = userService.findUserByLogin(login);
        if (user == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(new UserDTO(user));
    }

}

