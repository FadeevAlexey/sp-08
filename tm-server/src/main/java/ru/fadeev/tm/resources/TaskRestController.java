package ru.fadeev.tm.resources;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.entity.Task;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TaskRestController {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @GetMapping(value = "/api/tasks")
    public ResponseEntity<List<TaskDTO>> taskListGet(@NotNull final String userId) {
        @NotNull final List<TaskDTO> tasks = taskService.findAll(userId).stream()
                .map(TaskDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok(tasks);
    }

    @PostMapping(value = "/api/task")
    public ResponseEntity<TaskDTO> taskCreatePost(
            @NotNull final String userId,
            @RequestBody @NotNull final TaskDTO taskDTO
    ) {
        taskDTO.setUserId(userId);
        @NotNull final Task task = taskService.convertToTask(taskDTO);
        taskService.persist(task);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/api/task/{id}")
    public ResponseEntity<ProjectDTO> TaskRemoveGet(
            @NotNull final String userId,
            @PathVariable @NotNull final String id
    ) {
        taskService.remove(userId, id);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/api/task/{id}")
    public ResponseEntity<TaskDTO> taskViewGet(
            @NotNull final String userId,
            @PathVariable @NotNull final String id
    ) {
        @Nullable final Task task = taskService.findOne(userId, id);
        if (task == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(new TaskDTO(task));
    }

    @PutMapping(value = "/api/task")
    public ResponseEntity<TaskDTO> taskEditPost(
            @NotNull final String userId,
            @RequestBody @Nullable TaskDTO taskDTO
    ) {
        if (taskDTO == null) return ResponseEntity.badRequest().build();
        taskDTO.setUserId(userId);
        @NotNull final Task task = taskService.convertToTask(taskDTO);
        taskService.merge(task);

        return ResponseEntity.ok().build();
    }

}