package ru.fadeev.tm.specification;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.Specification;

public class Specifications {

    public static <T> Specification<T> findOne(@Nullable final String userId, @Nullable final String id) {
        return (root, cq, cb) -> cq.where(
                cb.equal(root.get("user").get("id"), userId),
                cb.equal(root.get("id"), id)
        ).getRestriction();
    }

    public static <T> Specification<T> findIdByName(@Nullable final String userId, @Nullable final String name) {
        return (root, cq, cb) -> cq.where(
                cb.equal(root.get("user").get("id"), userId),
                cb.equal(root.get("name"), name)
        ).getRestriction();
    }


    public static <T> Specification<T> findAllByUserId(@Nullable final String userId) {
        return (root, cq, cb) -> cq.where(cb.equal(root.get("user").get("id"), userId)).getRestriction();
    }


    public static <T> Specification<T> isLoginExist(@Nullable final String login) {
        return (user, cq, cb) -> cq.where(cb.equal(user.get("login"), login)).getRestriction();
    }

    public static <T> Specification<T> findUserByLogin(@NotNull final String login) {
        return (user, cq, cb) -> cq.where(cb.equal(user.get("login"), login)).getRestriction();
    }

}