package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IService;
import ru.fadeev.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    @Override
    public abstract List<E> findAll();

    @Nullable
    @Override
    public abstract E findOne(@Nullable String id);

    @Override
    public abstract void remove(@Nullable String id);

    @Override
    public abstract void persist(@Nullable E e);

    @Override
    public abstract void merge(@Nullable E e);

}