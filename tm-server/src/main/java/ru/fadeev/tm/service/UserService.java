package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.entity.Role;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.specification.Specifications;

import java.util.List;

@Service
@Transactional
public class UserService implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @Override
    public @NotNull List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Nullable
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        userRepository.deleteById(id);
    }

    @Override
    public void persist(@Nullable final User user) {
        if (user == null) return;
        userRepository.save(user);
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) return;
        userRepository.save(user);
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.findAll(Specifications.isLoginExist(login)).size() > 0;
    }

    @Override
    @Nullable
    public User findUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findAll(Specifications.findUserByLogin(login))
                .stream()
                .findFirst()
                .orElse(null);
    }

    public User convertToUser(@NotNull final UserDTO userDTO) {
        @Nullable User user = findOne(userDTO.getId());
        if (user == null) user = new User();
        user.setId(userDTO.getId());
        if (!userDTO.getLogin().isEmpty()) user.setLogin(userDTO.getLogin());
        @NotNull final Role role = new Role();
        role.setUser(user);
        if (user.getRoles().isEmpty()) user.getRoles().add(role);
        if (!userDTO.getPassword().isEmpty())
            user.setPasswordHash(new BCryptPasswordEncoder().encode(userDTO.getPassword()));
        if (userDTO.getEmail() != null && !userDTO.getEmail().isEmpty()) user.setEmail(userDTO.getEmail());
        return user;
    }

}