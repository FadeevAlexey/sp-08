package ru.fadeev.tm.util;

import ru.fadeev.tm.entity.Role;
import ru.fadeev.tm.enumerated.RoleType;

public class TestUtil {

    public static ru.fadeev.tm.entity.User admin() {
        ru.fadeev.tm.entity.User admin = new ru.fadeev.tm.entity.User();
        admin.setLogin("Admin");
        admin.setId("9070cf94-9500-4bf2-bc0e-ece81dd76870");
        admin.setPasswordHash("$2a$10$aJFwoRVcvvR2jTrf.0uh7OB0kGEexiMYW2VexzhLvtVUZ3F8QrCd.");
        Role role = new Role();
        role.setRole(RoleType.ADMINISTRATOR);
        role.setUser(admin);
        admin.getRoles().add(role);
        return admin;
    }

    public static ru.fadeev.tm.entity.User user() {
        ru.fadeev.tm.entity.User user = new ru.fadeev.tm.entity.User();
        user.setId("951f973c-8ae3-42eb-b7c4-0c4b3ef44161");
        user.setLogin("User");
        user.setPasswordHash("$2a$10$7zy3NjrcGclhf7o6RBoO5OfBovP2iceAFikMB4sq5FLKGFRWEVM7S");
        Role roleUser = new Role();
        roleUser.setRole(RoleType.USER);
        roleUser.setUser(user);
        user.getRoles().add(roleUser);
        return user;
    }

}
