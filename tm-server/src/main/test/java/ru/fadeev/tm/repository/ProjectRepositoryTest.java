package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.specification.Specifications;

import java.util.UUID;

import static org.junit.Assert.*;

public class ProjectRepositoryTest extends AbstractRepository {

    @Autowired
    IProjectRepository projectRepository;

    @Test
    public void saveProjectTest() {
        @Nullable Project project = new Project();
        project.setName("test");
        projectRepository.save(project);
        assertNotNull(projectRepository.findById(project.getId()));
        projectRepository.deleteById(project.getId());
    }

    @Test
    public void deleteByIdProjectTest() {
        @Nullable Project project = new Project();
        project.setName("test");
        projectRepository.save(project);
        projectRepository.deleteById(project.getId());
        project = projectRepository.findById(project.getId()).orElse(null);
        assertNull(project);
    }

    @Test
    @Transactional
    public void deleteUserProject() {
        @Nullable User user = userRepository.findOne(Specifications.findUserByLogin("User")).orElse(null);
        @Nullable User admin = userRepository.findOne(Specifications.findUserByLogin("Admin")).orElse(null);
        @Nullable Project project = new Project();
        project.setName("test");
        project.setUser(user);
        projectRepository.saveAndFlush(project);
        projectRepository.removeByUser_IdAndId(admin.getId(), project.getId());
        assertEquals("test", project.getName());
        project = projectRepository.getOne(project.getId());
        projectRepository.removeByUser_IdAndId(user.getId(), project.getId());
        assertFalse(projectRepository.findById(project.getId()).isPresent());
    }

    @Test
    @Transactional
    public void getOneProjectTestUserId() {
        @Nullable User user = userRepository.findOne(Specifications.findUserByLogin("User")).orElse(null);
        @Nullable User admin = userRepository.findOne(Specifications.findUserByLogin("Admin")).orElse(null);
        @Nullable Project project = new Project();
        project.setName("test");
        project.setUser(user);
        projectRepository.saveAndFlush(project);
        assertFalse(projectRepository.findOne(Specifications.findOne(admin.getId(), project.getId())).isPresent());
        project = projectRepository.findOne(Specifications.findOne(user.getId(), project.getId())).orElse(null);
        assertEquals("test", project.getName());
        projectRepository.removeByUser_IdAndId(user.getId(), project.getId());
    }

    @Test
    public void findOneProjectTest() {
        @Nullable Project project = new Project();
        project.setName("test");
        projectRepository.saveAndFlush(project);
        project = projectRepository.findById(project.getId()).orElse(null);
        assertEquals("test", project.getName());
        projectRepository.deleteById(project.getId());
        assertFalse(projectRepository.findById(project.getId()).isPresent());
        assertFalse(projectRepository.findById(UUID.randomUUID().toString()).isPresent());
    }

    @Test
    public void findAllTest() {
        final int projectsCount = projectRepository.findAll().size();
        @NotNull Project project = new Project();
        @NotNull Project project2 = new Project();
        @NotNull Project project3 = new Project();
        projectRepository.save(project);
        projectRepository.save(project2);
        projectRepository.save(project3);
        final int finalCount = projectRepository.findAll().size();
        Assert.assertEquals(projectsCount, finalCount - 3);
        projectRepository.deleteById(project.getId());
        projectRepository.deleteById(project2.getId());
        projectRepository.deleteById(project3.getId());
    }

    @Test
    @Transactional
    public void findAllByUserIdTest() {
        @Nullable User user = userRepository.findOne(Specifications.findUserByLogin("User")).orElse(null);
        @Nullable User admin = userRepository.findOne(Specifications.findUserByLogin("Admin")).orElse(null);
        final int projectsCount = projectRepository.findAll(Specifications.findAllByUserId(user.getId())).size();
        @NotNull Project project = new Project();
        project.setUser(user);
        @NotNull Project project2 = new Project();
        project2.setUser(user);
        @NotNull Project project3 = new Project();
        project3.setUser(user);
        projectRepository.save(project);
        projectRepository.save(project2);
        projectRepository.save(project3);
        final int finalCount = projectRepository.findAll(Specifications.findAllByUserId(user.getId())).size();
        Assert.assertEquals(projectsCount, finalCount - 3);
        @Nullable Project projectAdminTest = projectRepository.findOne(Specifications.findOne(admin.getId(), project.getId())).orElse(null);
        assertNull(projectAdminTest);
        projectRepository.deleteById(project.getId());
        projectRepository.deleteById(project2.getId());
        projectRepository.deleteById(project3.getId());
    }

    @Test
    public void mergeProjectTest() {
        Project project = new Project();
        project.setName("Igor");
        projectRepository.save(project);
        assertEquals("Igor", projectRepository.findById(project.getId()).orElse(null).getName());
        project.setName("Vitalik");
        projectRepository.save(project);
        assertEquals("Vitalik", projectRepository.findById(project.getId()).orElse(null).getName());
        projectRepository.deleteById(project.getId());
    }

    @Test
    @Transactional
    public void removeAllProjectTest() {
        @Nullable User user = userRepository.findOne(Specifications.findUserByLogin("User")).orElse(null);
        @NotNull Project project = new Project();
        project.setUser(user);
        projectRepository.save(project);
        @NotNull Project project2 = new Project();
        project2.setUser(user);
        projectRepository.save(project2);
        assertTrue(projectRepository.findAll(Specifications.findAllByUserId(user.getId())).size() > 1);
        projectRepository.removeAllByUser_Id(user.getId());
        assertTrue(projectRepository.findAll(Specifications.findAllByUserId(user.getId())).isEmpty());
    }

}