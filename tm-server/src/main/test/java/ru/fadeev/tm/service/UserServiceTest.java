package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.entity.User;

import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class UserServiceTest extends AbstractServiceTest {

    @Test
    public void persistUserTest() {
        @NotNull User user = new User();
        user.setLogin("testUser");
        userService.persist(user);
        assertNotNull(userService.findOne(user.getId()));
        userService.remove(user.getId());
    }

    @Test
    public void removeUserTest() {
        @Nullable User user = new User();
        user.setLogin("testUser");
        userService.persist(user);
        userService.remove(user.getId());
        user = userService.findOne(user.getId());
        assertNull(user);
    }

    @Test
    public void findOneUserTest() {
        @Nullable User user = userService.findUserByLogin("User");
        assertEquals("User", user.getLogin());
        assertNull(userService.findUserByLogin(null));
        assertNull(userService.findUserByLogin(UUID.randomUUID().toString()));
    }

    @Test
    public void findAllUserTest() {
        final int usersCount = userService.findAll().size();
        @NotNull User user = new User();
        user.setLogin(UUID.randomUUID().toString());
        @NotNull User user2 = new User();
        user.setLogin(UUID.randomUUID().toString());
        userService.persist(user);
        userService.persist(user2);
        final int finalCount = userService.findAll().size();
        Assert.assertEquals(usersCount, finalCount - 2);
        userService.remove(user.getId());
        userService.remove(user2.getId());
    }

    @Test
    public void convertToUserTest() {
        UserDTO userDTO = new UserDTO();
        userDTO.setLogin(UUID.randomUUID().toString());
        userDTO.setEmail("mail@mail.mail");
        User user = userService.convertToUser(userDTO);
        assertEquals(userDTO.getId(), user.getId());
        assertEquals(userDTO.getLogin(), user.getLogin());
        assertEquals(userDTO.getEmail(), user.getEmail());
        assertEquals("USER", user.getRoles().get(0).getRole().toString());
    }

    @Test
    public void mergeUserTest() {
        User user = new User();
        user.setLogin("Igor");
        userService.persist(user);
        assertEquals("Igor", userService.findOne(user.getId()).getLogin());
        user.setLogin("Vitalik");
        userService.merge(user);
        assertEquals("Vitalik", userService.findOne(user.getId()).getLogin());
        userService.remove(user.getId());
    }

    @Test
    public void isLoginExistTest() {
        assertTrue(userService.isLoginExist("Admin"));
        assertTrue(userService.isLoginExist("User"));
        assertFalse(userService.isLoginExist(UUID.randomUUID().toString()));
    }

    @Test
    public void findUserByLogin() {
        assertEquals("Admin", userService.findUserByLogin("Admin").getLogin());
        assertEquals("User", userService.findUserByLogin("User").getLogin());
        assertNull(userService.findUserByLogin(null));
        assertNull(userService.findUserByLogin(UUID.randomUUID().toString()));
    }

}