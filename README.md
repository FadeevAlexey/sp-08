###### [https://gitlab.com/FadeevAlexey/sp-08](https://gitlab.com/FadeevAlexey/sp-08)
# Task Manager 1.1.8

A simple web task manager, can help you organize your tasks.

### Built with
  - Java 8
  - Maven 4.0
  - Spring web MVC 5.0.8
  - Hibernate 5.4
  - Tomcat 7.0.47
  
### Developer
Alexey Fadeev
[alexey.v.fadeev@gmail.com](mailto:alexey.v.fadeev@gmail.com?subject=TaskManager)

### Building from source

```sh
$ git clone http://gitlab.volnenko.school/FadeevAlexey/sp-08.git
$ cd sp-08
$ mvn clean
$ mvn install
```

### Running Eureka-server

```sh
$ cd eureka-server
$ mvn spring-boot:run
```

### Running Config-server

```sh
$ cd config-server
$ mvn spring-boot:run
```

### Running Proxy-gateway

```sh
$ cd proxy-gateway
$ mvn spring-boot:run
```

### Running Server

```sh
$ cd tm-server
$ mvn spring-boot:run
```

### Running Client

```sh
$ cd tm-client
$ mvn spring-boot:run
```